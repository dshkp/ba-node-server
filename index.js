var express = require('express')
var app = express()

app.get('/test-simple', function (req, res) {
  res.send('Hello, World!')
})

// let fib = (function(cache){
//     return cache = cache || {}, function(n){
//         if (cache[n]) return cache[n]
//         else return cache[n] = n == 0 ? 0 : n < 0 ? -fib(-n)
//             : n <= 2 ? 1 : fib(n-2) + fib(n-1)
//     }
// })()

function Y(dn) {
    return (function(fn) {
        return fn(fn)
    }(function(fn) {
        return dn(function() {
            return fn(fn).apply(null, arguments)
        })
    }))
}
let fib = Y(function(fn) {
    return function(n) {
        if (n === 0 || n === 1) {
            return n
        }
        return fn(n - 1) + fn(n - 2)
    }
})

app.get('/fib/:number', function (req, res) {
  let number = req.params.number
  res.send(`${fib(parseInt(number))}`)
})

app.listen(3000)
